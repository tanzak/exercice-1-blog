import { Component } from '@angular/core';

class Post {
  private title: string;
  private content: string;
  private loveIts: number;
  private created_at: Date;

  constructor(title, content, loveIts) {
    this.title = title;
    this.content = content;
    this.loveIts = loveIts;
    this.created_at = new Date();

  }
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'blog';
  post1 = new Post('Mon premier Post', 'Ceci est le poste numéro 1 de ce blog', 0);
  post2 = new Post('Mon deuxiéme Post', 'Le deuxième post concerne l\'actualité concernant Angular 7', 0);
  post3 = new Post('Mon troisième Post', 'Le troisième post risque d\'être très court!', 0);
  constructor() {
  }
  posts = [this.post1, this.post2, this.post3];



}
